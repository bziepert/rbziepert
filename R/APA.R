
bz_apa <- function(data, row_names = FALSE, rname = "Rows"){

  data <- as.data.frame(data)
  
  if (row_names) {
  
    data[,rname] <- row.names(data)
    data <- data[,unique(c(rname, names(data)))]
      
  }
  
  table <- apaStyle::apa.table(
    data = data,
    level1.header = names(data),
    save = FALSE
  )$table

  return(table)
      
}

bz_apa_pca <- function(model, i_names) {
  
  # Get items
  
  table <- unclass(loadings(pc_rotated))
  
  table <- fa.sort(table)
  
  # table <- ifelse(
  #   abs(table) >= 0.4,
  #   paste0("<b>", formatC( table, format = 'f', digits = 2 ),"</b>"),
  #   formatC( table, format = 'f', digits = 2 )
  # ) 
  
  table <- ifelse(
    abs(table) >= 0.4,
    paste0("<b>", bz.round(table, pform = TRUE),"</b>"),
    bz.round(table, pform = TRUE)
  )
  
  # Add items row
  
  Item <- row.names(table)
  table <- cbind(table, Item)
  table <- table[ , unique(c("Item", colnames(table))) ]
  
  # Rename items
  
  for(i in 1:nrow(table)){
    
    name <- table[i,1]
    label <- i_names[ i_names[,1] == name , 2]
    table[i,1] <- label
    
  }
  
  # Add stats
  
  Eigenvalues <- bz.round(pc_rotated$Vaccounted["SS loadings",],2)
  Per_var <- bz.round(pc_rotated$Vaccounted["Proportion Var",],2)
  
  Eigenvalues <- c("Eigenvalues",Eigenvalues)
  Per_var <- c("% of variance",Per_var)
  
  table <- rbind(table, Eigenvalues)
  table <- rbind(table, Per_var)
  
  # Format table
  
  apa_table <- bz_apa(table, row_names = FALSE)
  
  return(apa_table)
  
}

bz_apa_structure_matrix <- function(model, i_names){
  
  # Get matrix
  
  table <- fa.sort(model$loadings %*% model$Phi)
  
  # table <- ifelse(
  #   abs(table) >= 0.4,
  #   paste0("<b>", formatC( table, format = 'f', digits = 2 ),"</b>"),
  #   formatC( table, format = 'f', digits = 2 )
  # )
  
  table <- ifelse(
    abs(table) >= 0.4,
    paste0("<b>", bz.round(table, pform = TRUE),"</b>"),
    bz.round(table, pform = TRUE)
  )
  
  # Add items row
  
  Item <- row.names(table)
  table <- cbind(table, Item)
  table <- table[ , unique(c("Item", colnames(table))) ]
  
  # Rename items
  
  for(i in 1:nrow(table)){
    
    name <- table[i,1]
    label <- i_names[ i_names[,1] == name , 2]
    table[i,1] <- label
    
  }
  
  # Format table
  
  apa_table <- bz_apa(table)
  
  return(apa_table)
  
}

bz.apa.codebook.crename <- function(table, codebook) {
  
  rename <- codebook[, c("Name", "Label")]
  
  colnames <- colnames(table)
  
  for (i in 1:nrow(rename)) {
    
    colnames[ colnames == rename[i,1] ] <- rename[i,2]
    
  }
  
  colnames(table) <- colnames
  
  return(table)
  
}

bz.apa.mean.sd.cor <- function(
  x, alpha = 0.05, type = "stars", coln = FALSE, codebook = list(),
  style = "apa", sep = " "
){ 
  
  # Check for codebook renaming

  if (length(codebook) != 0) {
    x <- bz.apa.codebook.crename(table = x, codebook = codebook)
  }
  
  # Get correlations matrix
  
  x <- as.matrix(x) 
  R <- Hmisc::rcorr(x)$r 
  p <- Hmisc::rcorr(x)$P
  
  # Create new table
  
  table <- matrix(bz.round(R, 2), ncol = ncol(x))
  table[upper.tri(table)] <- ""
  
  # Mark significant p values
  
  if (type == "bold") {
    
    p.mask <- abs(p) <= alpha
    p.mask[upper.tri(p.mask, diag = TRUE)] <- FALSE
    table <- ifelse(p.mask, paste0("<b>",table,"</b>"), table)  
    
  }
  
  if (type == "stars") {
    
    p.stars <- ifelse(
      abs(p) < .001, "***", ifelse(
        abs(p) < .01, "** ", ifelse(
          abs(p) < .05, "* ", " "
        )
      )
    )
    p.stars[upper.tri(p.stars, diag = TRUE)] <- ""
    table <- matrix( paste0(table, p.stars), ncol = ncol(x))
    
  }
  
  if (type == "p") {
    
    p.con <- bz.round(p, pform = TRUE)
    p.con <- paste0(sep, "(", p.con, ")")
    p.con <- matrix(p.con, nrow = nrow(table))
    p.con[upper.tri(p.con, diag = TRUE)] <- ""
    
    table.p <- paste0(table,p.con)
    table.p <- matrix(table.p, nrow = nrow(table))
    table.p[upper.tri(table.p)] <- ""
    
    p.mask <- abs(p) <= alpha
    p.mask[upper.tri(p.mask, diag = TRUE)] <- FALSE
    table <- ifelse(p.mask, paste0("<b>",table.p,"</b>"), table.p)
    
  }
  
  # Restore cnames
  
  colnames(table) <- paste(colnames(x), "", sep = "") 
  
  # Update variable names
  
  Variable <- colnames(table)
  
  if (coln) {
    colnames(table) <- sprintf("%02d", 1:ncol(x))
    Variable <- paste( sprintf("%02d", 1:ncol(x)), Variable)
  }
  
  table <- cbind(Variable, table)  
  
  if (type == "p") {
    colnames(table)[-1] <- paste0(
      colnames(table)[-1], "<br><i>R</i>", sep, "(<i>p</i>)"
    )
  }
  
  # Add SD
  
  SD <- matrixStats::colSds(as.matrix(x), na.rm = TRUE)
  SD <- bz.round(SD, round = 2)  
  table <- cbind(SD, table)
  
  # Add mean
  
  Mean <- colMeans(x, na.rm = TRUE)
  Mean <- bz.round(Mean,2)
  table <- cbind(Mean, table)
  
  # Convert to APA
  
  table <- table[,unique(c("Variable", colnames(table)))]

  if (style == "table") {
    return(table)
  }
  
  html.title <- paste0(
    "<div style='margin:1em 0 1em 0;'>",
    "<i>Mean, SD and Correlation Coefficients Table</i>",
    "</div>"
  )
  
  html.note <- paste0(
    "<div style='margin-top:1em;'>",
    "<i>Note.</i> No chance correction applied. ",
    "* <i>p</i> < .05. ** <i>p</i> < 0.01. *** <i>p</i> < 0.001.</div>"
  )
  
  apa <- bz.apa(table, html.title, html.note)
  
  # Convert
  
  apa <- bz.html(apa)
  
  # Return table
  
  return(apa)
  
}

bz.apa <- function(table, html.title, html.note) {
  
  table <- as.matrix(table)
  
  # Get th
  
  th.style <- paste("style='",
      "font-weight:normal;",
      "border-top: 1px solid black;",
      "border-bottom: 1px solid black;",
  "'")
  
  th <- paste0( "<th ",th.style,">", colnames(table), "</th>" )
  tr <- paste0("<tr>",paste0(th, collapse = ""),"</tr>")

  # Add td
  
  td.style <- "style='padding: 5px;'"
  
  td <- matrix(
    paste0("<td ", td.style, ">", table, "</td>"),
    ncol = ncol(table)
  )
  
  # Add tr
  
  for (i in 1:nrow(table)) {
    
    tr <- paste0(tr, "<tr>",paste0(td[i,], collapse = ""),"</tr>")
    
  }
  
  # Add table
  
  html.table <- paste0(
    "<table style='border-bottom:1px solid black;' cellspacing='0'>",
    tr,
    "</table>"
  )
  
  # Add note
  
  html <- paste(html.title, html.table, html.note)
  
  # Return html
  
  return(html)
    
}
