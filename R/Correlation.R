
bz_rcorr_stat <- function(data) {
  
  # Print test results
  print(data)
  
  # Get stats
  r <- data$r[1,2]
  n <- data$n[1,2]
  p <- data$P[1,2]
  
  # Stats
  cat("\nInterpretation support:\n\n")
  
  if (abs(r) < 0.3) {
    message("- R below 0.3")
  } else {
    cat("- R above 0.3\n")
  }
  
  if (p > 0.05) {
    message("- p above 0.05")
  } else {
    cat("- p below 0.05\n")
  }
  
}

bz_rcorr_which <- function(
  data, p_cut = 0.05, r_cut = 0.3, bonferroni = TRUE
) {
  
  # Extract correlation table
  r <- data$r
  
  # Stats
  size <- nrow(r)
  cor_count <- (size ^ 2 - size)/2
  
  # Bonferroni correction
  if (bonferroni) {
    p_cut <- p_cut / cor_count
  }
  
  # Remove values above P value
  r[ data$P >= p_cut ] <- 0
  r[ is.na(data$P) ] <- 0
  r[ lower.tri(r) ] <- 0
  
  # Remove values below R value
  r[ abs(r) < r_cut ] <- 0
  
  # Count matches
  Count <- sum(abs(r) != 0)
  
  if (Count == 0) {
    
    index <- "No matches found"
    
  } else {
    
    # Get matching values
    index <- which( abs(r) != 0 , arr.ind = TRUE)
    rnames <- row.names(r)
    cnames <- colnames(r)
    row.names(index) <- NULL
    index <- as.data.frame(index)
    
    # List matching values
    for (i in 1:nrow(index)) {
      
      rown <- as.integer(index[i,"row"])
      coln <- as.integer(index[i,"col"])
      
      index[i,"row"] <- rnames[as.integer(index[i,"row"])]
      index[i,"col"] <- cnames[as.integer(index[i,"col"])]
      index[i,"r"] <- data$r[rown, coln]
      index[i,"p"] <- data$P[rown, coln]
      
    }
    
  }
  
  # Info
  
  cat("\nNumber of correlations =", cor_count,"\n")
  cat("P-level =", p_cut,"\n")
  cat("Bonferroni correction used =", bonferroni,"\n")
  cat("R-level =", r_cut,"\n")
  cat("Number of significant correlations =", Count,"\n")
  
  return(index)
  
}

bz_cor_which <- function(
  data,
  row_sel = names(data),
  col_sel = names(data),
  r_cut = 0.3,
  p_cut = 0.05,
  r_round = 3,
  p_round = 3
){
  
  # Prepare
  require(reshape2)
  data <- as.matrix(data)
  
  # Correlate
  cor <- Hmisc::rcorr(data)
  r <- cor$r
  p <- cor$P
  
  # Select rows and columns
  r <- r[ row.names(r) %in% row_sel, col_sel ]
  p <- p[ row.names(p) %in% row_sel, col_sel ]
  
  # Round
  r <- round(r, r_round)
  p <- round(p, p_round)
  
  # Melt
  meltr <- reshape2::melt(r, value.name = "r")
  meltp <- reshape2::melt(p, value.name = "p")
  
  melt <- meltr
  melt[,"p"] <- meltp[,"p"]
  
  # Cut
  melt <- melt[ abs(melt[,"r"]) >= r_cut & abs(melt[,"p"]) <= p_cut, ]
  
  # Return
  return(melt)
  
}

bz_correlation_chart <- function(data) {
  
  data <- as.matrix(data)
  
  suppressWarnings(
    PerformanceAnalytics::chart.Correlation(
      data,
      histogram = TRUE,
      pch = 19)
  )
  
}

bz_corstarsl <- function(x, r_cut = 0){ 
  
  # Prepare
  require(Hmisc) 
  x <- as.matrix(x) 
  R <- rcorr(x)$r 
  p <- rcorr(x)$P
  
  # define notions for significance levels; spacing is important.
  mystars <- ifelse(
    p < .001, "***", ifelse(p < .01, "** ", ifelse(p < .05, "* ", " "))
  )
  
  # trunctuate the matrix that holds the correlations to two decimal
  R <- format(round(cbind(rep(-1.11, ncol(x)), R), 2))[,-1] 
  
  # build a new matrix that includes the correlations with their apropriate
  # stars 
  Rnew <- matrix(paste(R, mystars, sep = ""), ncol = ncol(x)) 
  diag(Rnew) <- paste(diag(R), " ", sep = "") 
  rownames(Rnew) <- colnames(x) 
  colnames(Rnew) <- paste(colnames(x), "", sep = "") 
  
  # remove upper triangle
  Rnew <- as.matrix(Rnew)
  Rnew[upper.tri(Rnew, diag = TRUE)] <- ""
  Rnew <- as.data.frame(Rnew) 
  
  # remove last column and return the matrix (which is now a data frame)
  Rnew <- cbind(Rnew[1:length(Rnew) - 1])
  return(Rnew) 
}

bz_corstar_select <- function(data, x_cnames, y_cnames, r_cut = 0) {
  
  table <- bz_corstarsl(data[,c(x_cnames, y_cnames)], r_cut)
  table <- table[ row.names.data.frame(table) %in% y_cnames, x_cnames ]
  return(table)
  
}

