
# Include markdown

bz.markdown <- function(md) {
  
  md.file <- tempfile(fileext = ".Rmd")
  md.page <- paste0(md, "\n")
  cat(md.page, file = md.file)
  html <- htmltools::includeMarkdown(md.file)
  return(html)
  
}